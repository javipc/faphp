# FA PHP

Este programa sirve para mostrar gráficos en una pagina web PHP, 
creados por Fontawesome pero sin la necesidad de javascript, de css ni de ningún otro complemento, 
ésto genera un impacto favorable en el rendimiento ya que no necesita cargar scripts en el navegador 
para mostrar un par de imágenes.



# Características

* Podes controlar la dimensión y el color directamente.
* Podes personalizarlo con css (colores, dimensiones).
* Permite estilos alternativos.
* No requiere jQuery.
* No requiere boostrap.
* No requiere complementos externos.
* No utiliza javascript.
* No utiliza Apis de sitios externos.
* No utiliza código propietario.
* Es un único archivo PHP.
* No requiere fuentes.
* No envía archivos (fuentes, imágenes, scripts) se ejecuta en el servidor.
* Las imágenes son en formato SVG incrustadas en el documento HTML.
* Ofrece la etiqueta lista para usar.
* Ofrece información para crear tu propia etiqueta SVG.
* Es simple, solo dos funciones, crear y mostrar.
* Ofrece posibilidad de múltiples colores y tamaños aleatorios.
* Ofrece posibilidad de animación (sin javascript ni css).
* Es LIBRE.



# Instalación
Muy simple, un único archivo

require_once("faphp.php");

# Cómo se usa

Crear el objeto
$grafico = new faphp ();

Mostrar la imagen
$grafico->mostrar ('heart');

Si querés mas información [visita la página](https://javim.000webhostapp.com/) del proyecto.

# Créditos

Este programa se basa en fuentes de https://fontawesome.com

			


# Más aplicaciones

Al no tener publicidad este proyecto se mantiene únicamente con donaciones.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



